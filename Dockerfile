# front-end-fabian container dockerfile #
FROM nginx:1.18.0-alpine

ARG DIST_FOLDER=dist/

COPY .nginx/default.conf /etc/nginx/conf.d/default.conf
COPY ${DIST_FOLDER} /usr/share/nginx/html

EXPOSE 80
