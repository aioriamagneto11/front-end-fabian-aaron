/* eslint-disable no-console */
import axios from 'axios';
import sha256 from 'js-sha256';

const API_URL = 'http://localhost:6969/v1/';

class AuthService {
  login(user) {
    const qs = require('querystring');

    const requestBody = {
      user: user.username,
      password: user.password
    };

    const config = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    };

    return axios
      .post(API_URL + 'login', qs.stringify(requestBody), config)
      .then(response => {
        if (response.data.token) {
          localStorage.setItem('user', JSON.stringify(response.data));
          localStorage.setItem('token', response.data.token);
        }
        return response.data;
      });
  }

  logout() {
    localStorage.removeItem('user');
  }

  register(user) {
    return axios.post(API_URL + 'alumnos', {
      correo: user.email,
      division: user.division,
      groups: [],
      matricula: user.username,
      posts: [],
      token: sha256(user.password)
    });
  }
}

export default new AuthService();
